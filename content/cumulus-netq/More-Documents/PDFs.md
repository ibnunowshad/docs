---
title: PDFs
author: Cumulus Networks
weight: 306
product: Cumulus NetQ
draft: false
version: 2.3
imgData: cumulus-netq
siteSlug: cumulus-netq
---
The following Cumulus NetQ user documentation is available in PDF for offline viewing or printing:

NetQ 2.3.0

- [Cumulus NetQ Deployment Guide PDF](https://dkahegywkrw3e.cloudfront.net/pdfs/Cumulus-NetQ-Deployment-Guide-230.pdf)
- [Cumulus NetQ Integration Guide PDF](https://dkahegywkrw3e.cloudfront.net/pdfs/Cumulus-NetQ-Integration-Guide-230.pdf)
- [Cumulus NetQ UI User Guide PDF](https://dkahegywkrw3e.cloudfront.net/pdfs/Cumulus-NetQ-UI-User-Guide-230.pdf)
- [Cumulus NetQ CLI User Guide PDF](https://dkahegywkrw3e.cloudfront.net/pdfs/Cumulus-NetQ-CLI-User-Guide-230.pdf)

{{%notice note%}}

Many command line examples have very wide output which can compromise readability in the above documents.

{{%/notice%}}
